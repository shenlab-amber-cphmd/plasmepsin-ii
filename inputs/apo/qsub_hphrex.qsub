#!/usr/bin/tcsh
#$ -S /bin/tcsh
#$ -cwd
#$ -V
#$ -N A.PLASME
#$ -o $JOB_ID.o
#$ -e $JOB_ID.e
#$ -j y
#$ -l h_data=500M,h_rt=180:00:00
#$ -pe ppn64 192
##$ -pe mpirun1 32
#$ -R y

#Production should be run with 256
### Prepare environment. Do not change.
# Due to a bug in SGE qrsh will dump core if it exits immediately.
limit coredumpsize 4
limit cputime      unlimited
limit filesize     unlimited
limit datasize     unlimited
limit stacksize    unlimited
limit memoryuse    unlimited
limit vmemoryuse   unlimited

# Change this for each job
set pdbname = plasmepsin
set temp    = 300.0
set conc    = 0.15 
set struct  = ${pdbname}

# This is environment dependent 
set rundir = /home/$USER/simulations/apo_plasmepsin/run
set workdir = `pwd`

# Update this for restarts
set mystage = 11      #equilibration, >0: production run

# Don't need to change this
set lstage = `echo $mystage 1 | awk '{print $1-$2}'`
set nrep = 0 # counter for all reps

if ( $mystage == 0 ) then
  set input = ${pdbname}_equilibration
  mkdir $rundir
  cp ${struct}.* $rundir/.
  sed "s/PDBNAME/$pdbname/g;s/TEMPERATURE/$temp/g;s/CONCENTRATION/$conc/g" equilibration_hphmd.inp > $rundir/$input.inp
else

  set input = ${pdbname}_ephrex

  # for each replica set pH, and four unique random number seeds
  foreach ph ( 1.0 1.25 1.5 1.75 2.0 2.25 2.5 2.75 3.0 3.25 3.5 3.75 4.0 4.25 4.5 4.75 5.0 5.5 6.0 6.5 7.0 7.5 8.0 8.5 )
      set mys1 = `/home/wei/local/bin/gawk 'BEGIN { srand(systime() + PROCINFO["pid"]); print int(rand()*10000000)}'`
      set mys2 = `/home/wei/local/bin/gawk 'BEGIN { srand(systime() + PROCINFO["pid"]); print int(rand()*10000000)}'`
      set mys3 = `/home/wei/local/bin/gawk 'BEGIN { srand(systime() + PROCINFO["pid"]); print int(rand()*10000000)}'`
      set mys4 = `/home/wei/local/bin/gawk 'BEGIN { srand(systime() + PROCINFO["pid"]); print int(rand()*10000000)}'`

      sed "s/REPPH/$ph/g;s/REPTEMP/$temp/g;s/JOB/$pdbname/g;s/REP/$nrep/g;s/S1/$mys1/g;s/S2/$mys2/g;s/S3/$mys3/g;s/S4/$mys4/g" rep.cmd > $rundir/rep.cmd_$nrep
     
      if ( $mystage == 1 ) then
         cp $rundir/stage0/${pdbname}_equil_4.rst $rundir/stage0/${pdbname}_ph${ph}_temp${temp}.rst_${nrep}
      endif

      @ nrep++
  end

  sed "s/PDBNAME/$pdbname/g;s/TEMPERATURE/$temp/g;s/CONCENTRATION/$conc/g;s/STAGE/$mystage/g;s/NREP/$nrep/g" hphrex.inp > $rundir/$input.inp

endif

cd $rundir
mkdir stage$mystage

### Job-specific parameter. Re-evaluate for every job.
set cmdline = "/home/yhuang/software/charmm/c42a2_ephmd_pme_dph_volt/exec/gnu_M/charmm"

### Implementation-specific parameter. Set this only once. OpenMPI
### doesn't need a machine file, but requires --leave-session-attached
set mpirun = "mpirun --leave-session-attached"

### Submit job using mpirun. Do not change.
$mpirun -np $NSLOTS $cmdline < $input.inp >& $input.out

### Clean up scratch
 
exit
