-- MD Software --

CHARMM - Developmental Version 42a2

-- Analysis Library Versions ---

Python 3.7.6 
Pyemme 2.5.7
numpy 1.20.2
mdanalysis 1.1.1
jupyter 1.0.0

CPPTRAJ: Version V5.0.1 (GitHub)

-- Contents of Project --

Here, all input files for running the equilibration and production can be found along with the scripts used for analysis and plotting the data for each figure in the paper including the SI. 



